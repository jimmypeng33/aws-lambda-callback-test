exports.handler = async function(event, context, callback) {
    function AccountAlreadyExistsError(message) {
        this.name = "AccountAlreadyExistsError";
        this.message = message;
    }
    AccountAlreadyExistsError.prototype = new Error();
 
    const error = new AccountAlreadyExistsError("Account is in use!");
    throw error
}